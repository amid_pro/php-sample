<?php

class Application {

    function __construct() {

        $query = filter_input(INPUT_GET, 'query', FILTER_SANITIZE_SPECIAL_CHARS);

        $controller = new Controller();

        if (!$query) {
            $controller->index();
        } else {

            $path = explode("/", $query);
            $method = $path[0];

            $method_args = [];
            for ($i = 1; $i < count($path); $i++) {
                if ($path[$i] !== "") {
                    $method_args[] = $path[$i];
                }
            }

            if (method_exists($controller, $method)) {
                call_user_func_array([$controller, $method], $method_args);
            } else {
                $controller->error(404);
            }
        }
    }

}
