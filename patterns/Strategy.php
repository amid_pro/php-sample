<?php

namespace patterns\strategy;

interface StrategyInterface {
    public function paymentOrder();
}

class Strategy {
    
    private $payment;
    
    public function setPaymeny(StrategyInterface $payment){
        $this->payment = $payment;
    }
    
    public function paymentProcess(){
        return $this->payment->paymentOrder();
    }
    
}


class StrategyPaymentOne implements StrategyInterface {
    public function paymentOrder() {
        return __CLASS__;
    }
}

class StrategyPaymentTwo implements StrategyInterface {
    public function paymentOrder() {
        return __CLASS__;
    }
}
