<?php

namespace patterns\facade;

class FacadeItem {
    
    public function getPrice(){
        return 1000;
    }
    
    public function getName(){
        return __METHOD__;
    }
    
    public function getDescription(){
        return __CLASS__;
    }
}


class Facade {
    
    private $item;
    
    public function __construct(FacadeItem $item) {
        $this->item = $item;
    }
    
    public function getItem(){
        $out = "Item name: " . $this->item->getName() . PHP_EOL;
        $out.= "Item description: " . $this->item->getDescription() . PHP_EOL;
        $out.= "Item price: " . $this->item->getPrice() . PHP_EOL;
        return $out;
    }
    
}