<?php

namespace patterns\chainofresponsibilities;

interface ChainInterface {
    public function getName();
    public function getPrice();
}

class ChainItem1 implements ChainInterface {
    public function getPrice() {
        return 100;
    }
    public function getName() {
        return __CLASS__;
    }
}

class ChainItem2 implements ChainInterface {
    public function getPrice() {
        return 200;
    }
    public function getName() {
        return __CLASS__;
    }
}

class ChainItem3 implements ChainInterface {
    public function getPrice() {
        return 300;
    }
    public function getName() {
        return __CLASS__;
    }
}


class ChainOfResponsibilities {
    
    private $items = [];
    private $buyingItem = FALSE;
    
    public function addItemVariable(ChainInterface $item){
        if (!in_array($item, $this->items)){
            $this->items[] = $item;
        }
    }

    public function avialiblePay($price){
  
        $out = "";
        
        foreach ($this->items as $item){
            
            $out.= "Trying to buy " . $item->getName() . " on " . $item->getPrice() . PHP_EOL;
            
            if ($price >= $item->getPrice()){
                $out.= "Done. Buying " . $item->getName() . PHP_EOL;
                $this->buyingItem = $item;
                break;
            }
            
        }
        
        if (!$this->buyingItem){
            $out.= "Not enough money for buy" . PHP_EOL;
        }
        
        return $out;
    }
    
}

