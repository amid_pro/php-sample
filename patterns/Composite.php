<?php

namespace patterns\composite;

interface getItemComponent {
    public function get();
}

class ItemComponent1 implements getItemComponent {
    public function get(){
        return __METHOD__;
    }
}

class ItemComponent2 implements getItemComponent {
    public function get(){
        return __METHOD__;
    }
}


class Composite {
    
    private $components;
    
    public function addComponent(getItemComponent $component){
        $this->components[] = $component;
    }
    
    public function getItemCompleted(){
        $out = "";
        
        foreach ($this->components as $component){
            $out.= " " . $component->get();
        }
        
        return $out;
    }
}