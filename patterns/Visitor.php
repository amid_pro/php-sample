<?php

namespace patterns\visitor;

class VisitorClass {
    
    public function itemOne($name){
        return $name;
    }
    
    public function itemTwo($name){
        return $name;
    }
    
}


interface VisitorInterface {
    public function acceptVisitor(VisitorClass $visitor);
}

class VisitorItemOne implements VisitorInterface {
    
    private function getName() {
        return __METHOD__;
    }
    
    public function acceptVisitor(VisitorClass $visitor) {
        return $visitor->itemOne($this->getName());
    }
    
}

class VisitorItemTwo implements VisitorInterface {
    
    private function getName() {
        return __METHOD__;
    }
    
    public function acceptVisitor(VisitorClass $visitor) {
        return $visitor->itemTwo($this->getName());
    }
    
}


class Visitor {
    
    private $items = [];
    private $visitorClass;
    
    function __construct() {
        $visitorClass = new VisitorClass;
        $this->items[] = new VisitorItemOne;
        $this->items[] = new VisitorItemTwo;
        return $this->getItems();
    }
    
    private function getItems(){
        
        $out = "";
        
        foreach ($this->items as $item){
            $out.= $item->acceptVisitor(new VisitorClass);
        }
        
        return $out;
        
    }
    
}