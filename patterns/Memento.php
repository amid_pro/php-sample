<?php

namespace patterns\memento;

class MementoState{
    
    private $state;
    
    public function setState($order){
        $this->state = $order;
    }
    
    public function getState(){
        return $this->state;
    }
    
}

class Memento{
    
    private $items = [];
    private $memento;
    
    public function __construct() {
        $this->memento = new MementoState;
    }

    public function addItem($id){
        if (!in_array($id, $this->items)){
            $this->items[] = $id;
        }
    }
    
    public function saveState(){
        $this->memento->setState($this->items);
    }
    
    public function restoreState(){
        $this->items = $this->memento->getState();
    }
    
    public function getItems(){
        return $this->items;
    }
    
}
