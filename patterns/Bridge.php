<?php

namespace patterns\bridge;

interface BridgeItem {
    public function setPrice($price);
    public function getPrice();
}

interface BridgeLang {
    function i18nPrice($price);
}

class BridgeUsd implements \patterns\bridge\BridgeLang { 
    public function i18nPrice($price) {
        return $price * 60;
    }
}


class BridgeEur implements \patterns\bridge\BridgeLang {
    public function i18nPrice($price) {
        return $price * 70;
    }
}

class Bridge implements BridgeItem {
    
    private $lang;
    private $price;
            
    function __construct(\patterns\bridge\BridgeLang $lang) {
        $this->lang = $lang;
    }
    
    public function setPrice($price) {
        $this->price = $this->lang->i18nPrice($price);
    }

    public function getPrice() {
        return $this->price;
    }
    
}

