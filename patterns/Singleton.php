<?php

namespace patterns\singleton;

final class Singleton {
    
    private static $instance;

    private function __construct() {
        $this->id = uniqid();
    }
    
    public static function getInstance(){
        if (!self::$instance){
            self::$instance = new Singleton;
        }
        return self::$instance;
    }
    
    
    private function __clone() {}
    private function __wakeup() {}
    
}

