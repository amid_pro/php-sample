<?php

namespace patterns\mediator;

class MediatorItem{
    
    private $item = [];
    private $mediator;

    public function __construct($id, \patterns\mediator\Mediator $mediator) {
        $this->item['id'] = $id;
        $this->mediator = $mediator;
    }
    
    public function getItem(){
        return $this->mediator->sendItem($this->item);
    }
    
}

class Mediator{
    
    private function addItemFeature(){
        return uniqid();
    }

    public function sendItem($item){
        $item['feature'] = $this->addItemFeature();
        return $item;
    }
    
}
