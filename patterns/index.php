<?php

namespace patterns;

spl_autoload_register(function ($class_name) {
    require_once __DIR__ . DIRECTORY_SEPARATOR . end(explode(DIRECTORY_SEPARATOR, $class_name)) . '.php';
});


function dump($data){
    echo '<pre>';
    var_dump($data); 
    echo '</pre>';
}

// Factory
dump(factory\Factory::createProduct('Product1')->getItem());


// Builder
dump((new builder\Builder)->buildBigProduct()); !!


// Singleton
$singleton1 = singleton\Singleton::getInstance();
$singleton2 = singleton\Singleton::getInstance();
dump($singleton1 === $singleton2);


// Adapter
dump((new adapter\Adapter)->getClass());


// Composite
$composite = new composite\Composite;
$composite->addComponent(new composite\ItemComponent1);
$composite->addComponent(new composite\ItemComponent2);
dump($composite->getItemCompleted());


// Decorator
$decoratorItem = new decorator\Decorator;
$DecoratorItemWithFeature = new decorator\DecoratorFeature($decoratorItem);
dump($DecoratorItemWithFeature->getPrice());


// Facade
$facade = new facade\Facade(new facade\FacadeItem);
dump($facade->getItem());


// Proxy
$proxy = new proxy\Proxy(new proxy\ProxyItem());
dump($proxy->getItem(1000));


// ChainOfResponsibilities
$chainOfResponsibilities = new chainofresponsibilities\ChainOfResponsibilities;
$chainOfResponsibilities->addItemVariable(new chainofresponsibilities\ChainItem3);
$chainOfResponsibilities->addItemVariable(new chainofresponsibilities\ChainItem2);
$chainOfResponsibilities->addItemVariable(new chainofresponsibilities\ChainItem1);
dump($chainOfResponsibilities->avialiblePay(100));


// Command
$command = new command\Command(new command\CommandReceiver);
$command->setOn();
dump($command->showState());
$command->setOff();
dump($command->showState());


// Iterator
$iterator = new iterator\Iterator;
$iterator->addItem(new iterator\IteratorItem(1));
$iterator->addItem(new iterator\IteratorItem(255));
$iterator->addItem(new iterator\IteratorItem(12));
foreach ($iterator->getItems() as $item){
    dump($item->getId());
}


// Memento
$memento = new memento\Memento;
$memento->addItem(12);
$memento->addItem(25);
dump($memento->getItems());
$memento->saveState();
$memento->addItem(7);
dump($memento->getItems());
$memento->restoreState();
dump($memento->getItems());


// Observer
$observer = new observer\Observer;
$observer->addSubscribe(new observer\ObserverSubscriber);
$observer->addSubscribe(new observer\ObserverSubscriber);
$observer->updateSubscribers();


// State
$state = new state\State();
$state->setNextContext();
$state->setNextContext();


// Strategy
$strategy = new strategy\Strategy;
$strategy->setPaymeny(new strategy\StrategyPaymentOne);
dump($strategy->paymentProcess());
$strategy->setPaymeny(new strategy\StrategyPaymentTwo);
dump($strategy->paymentProcess());


// TemplateMethod
dump(new templatemethod\TemplateMethod());


// Visitor
dump(new visitor\Visitor());


// Mediator
$mediator = new mediator\Mediator;
$mediatorItem = new mediator\MediatorItem(55, $mediator);
dump($mediatorItem->getItem());


// Bridge
$bridgeProduct = new bridge\Bridge(new bridge\BridgeEur);
$bridgeProduct->setPrice(100);
dump($bridgeProduct->getPrice());


// Prototype
$prototype = new prototype\Prototype;
dump($prototype->getClone());