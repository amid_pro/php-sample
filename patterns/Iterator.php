<?php

namespace patterns\iterator;

class IteratorItem{
    private $id;
    
    public function __construct($id) {
        $this->id = $id;
    }
    
    public function getId(){
        return $this->id;
    }
}


class Iterator{
    
    private $items = [];
    
    public function getItems(){
        return $this->items;
    }
    
    public function addItem($id){
        if (!in_array($id, $this->items)){
            $this->items[] = $id;
        }
    }
    
    public function getCount(){
        return count($this->items);
    }
    
    public function removeItem($id){
        if (in_array($id, $this->items)){
            unset($this->items[$id]);
        }
    }

}