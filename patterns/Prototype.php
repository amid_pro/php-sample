<?php

namespace patterns\prototype;

class PrototypeItem {

    public function __construct() {
        $this->id = uniqid();
    }

    public function __clone() {}
}


class Prototype {
   
    public function __clone() {}

    public function getClone(){
        $class = new PrototypeItem;
        $item = clone $class; 
        return $item;
    }
    
}