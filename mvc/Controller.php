<?php

class Controller {

    private function render($action, $data = []) {
        extract($data);
        ob_start();
        include APP . 'view/' . $action . '.php';
        echo ob_get_clean();
    }

    public function error($code, $error = FALSE) {

        http_response_code($code);
        $error_message = $error ? $error : "Страница не найдена";

        $this->render("error", [
            "error_message" => $error_message
        ]);
        die();
    }

    public function index() {
        $this->render("index", [
            "time" => time(),
            "uniqid" => uniqid()
        ]);
    }

    public function users($id = NULL) {
        $model = new Model();

        if ($id && (int) $id > 0) {
            $user = $model->getUser($id);
            if (!$user) {
                $this->error(404, "Пользователь не найден");
            } else {
                $this->render("user", [
                    "user" => $user
                ]);
            }
        } else {
            $this->render("users", [
                "users" => $model->getAllUsers()
            ]);
        }
    }

    public function page($title = "Test page") {
        $this->render("page", [
            "title" => $title
        ]);
    }

}
