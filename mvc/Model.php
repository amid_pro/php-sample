<?php

class Model {

    private static $instance = NULL;

    const DB_NAME = '';
    const DB_USER = 'root';
    const DB_PASSWORD = '';
    const DB_HOST = '127.0.0.1';

    function __construct() {
        
    }

    private static function getInstance() {
        if (!isset(self::$instance)) {
            try {
                self::$instance = new PDO("mysql:dbname=" . self::DB_NAME . ";host=" . self::DB_HOST, self::DB_USER, self::DB_PASSWORD, [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
                ]);
            } catch (PDOException $error) {
                echo $error;
                die();
            }
        }
        return self::$instance;
    }

    public function getUser($id) {
        $user = self::getInstance()->prepare('SELECT user_name FROM users WHERE id = :id LIMIT 1');
        $user->bindParam(":id", $id, PDO::PARAM_INT);
        $user->execute();
        return $user->fetchAll(PDO::FETCH_COLUMN);
    }

    public function getAllUsers() {
        $users = self::getInstance()->prepare('SELECT id, user_name FROM users');
        $users->execute();
        return $users->fetchAll(PDO::FETCH_COLUMN);
    }

}
