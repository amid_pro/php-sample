<?php

namespace patterns\command;

interface CommandInterface {
    public function execute();
}

class CommandReceiver implements CommandInterface {
    
    private $state;
    
    public function __construct() {
        $this->state = FALSE;
    }

    public function execute() {
        $this->state = TRUE;
    }
    
    protected function undo(){
        $this->state = FALSE;
    }
    
    protected function redo(){
        $this->execute();
    }
    
    protected function getState(){
        return $this->state;
    }
    
}

class Command extends CommandReceiver {
    
    private $command;

    public function __construct(CommandInterface $command) {
        $this->command = $command;
    }

    public function setOn() {
        $this->command->redo();
    }
    
    public function setOff(){
        $this->command->undo();
    }
    
    public function showState(){
        return $this->command->getState();
    }
    
}
