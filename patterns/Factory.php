<?php

namespace patterns\factory;

interface Item {
    public function getItem();
}

class Product1 implements Item {
    public function getItem() {
        return __METHOD__;
    }
}

class Product2 implements Item {
    public function getItem() {
        return __METHOD__;
    }
}

class Product3 implements Item {
    public function getItem() {
        return __METHOD__;
    }
}

class Factory {
    
    public static function createProduct($product_name){
        $class = 'patterns\factory\\' . $product_name;
        if (class_exists ($class)){
            return new $class;
        }
        return FALSE;
    }
    
}

