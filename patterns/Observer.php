<?php

namespace patterns\observer;

interface ObserverInterface {
    public function updateState();
}

class Observer {
    
    private $subscribers = [];
    
    public function addSubscribe(ObserverInterface $subscribe){
        if (!in_array($subscribe, $this->subscribers)){
            $this->subscribers[] = $subscribe;
        }
    }
    
    public function updateSubscribers(){
        foreach ($this->subscribers as $subscriber){
            $subscriber->updateState();
        }
    }
    
}


class ObserverSubscriber implements ObserverInterface {
    
    private $name;
    
    function __construct() {
        $this->name = uniqid();
    }

    public function updateState() {
        echo "Update " . $this->name . PHP_EOL;
    }
    
}


