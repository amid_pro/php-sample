<?php

namespace patterns\decorator;

interface DecoratorItemInterface {
    public function getPrice();
}


class Decorator implements DecoratorItemInterface {
    public function getPrice() {
        return 1000;
    }
}


class DecoratorFeature implements DecoratorItemInterface {
    private $item;
    
    public function __construct(DecoratorItemInterface $item) {
        $this->item = $item;
    }
    
    public function getPrice() {
        return $this->item->getPrice() + 100;
    }
}

