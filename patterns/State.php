<?php

namespace patterns\state;

class State {
    
    private $context;
    
    public function __construct() {
        $this->setContext(new StateContextOne);
    }

    public function setContext(StateInterface $context){
        $this->context = $context;
        echo $this->context->getName() . PHP_EOL;
    }
    
    public function setNextContext(){
        if (NULL !== $this->context->nextContext()){
            $this->setContext($this->context->nextContext());
        }
    }
    
}


interface StateInterface {
    public function getName();
    public function nextContext();
}

class StateContextOne implements StateInterface {
    
    public function getName() {
        return __CLASS__ . PHP_EOL;
    }

    public function nextContext() {
        return new StateContextTwo;
    }
}


class StateContextTwo implements StateInterface {
    
    public function getName() {
        return __CLASS__ . PHP_EOL;
    }

    public function nextContext() {
        return new StateContextThree;
    }
}

class StateContextThree implements StateInterface {
    
    public function getName() {
        return __CLASS__ . PHP_EOL;
    }

    public function nextContext() {
        return NULL;
    }
}
