<?php

namespace patterns\proxy;

class ProxyItem {
    public function getItem(){
        return __METHOD__;
    }
}

class Proxy {
    
    private $item;
    
    private $price = 1000;

    public function __construct(ProxyItem $item) {
        $this->item = $item;
    }
    
    
    public function getItem($price){
        if ($this->price == $price){
            return $this->item->getItem();
        }
        return FALSE;
    }
    
}