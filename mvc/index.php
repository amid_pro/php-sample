<?php

define('APP', __DIR__ . DIRECTORY_SEPARATOR);
spl_autoload_register(function($className) {
    require APP . $className . '.php';
});
new Application();
