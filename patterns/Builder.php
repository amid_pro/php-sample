<?php

namespace patterns\builder;

class CreateItem {
    
    function __construct() {
        $this->product = array();
    }
      
    public function setName($name){
        $this->product['name'] = $name;
    }
    public function setPrice($price){
        $this->product['price'] = $price;
    }
    
    protected function setId($id){
        $this->product['id'] = $id;
    }

    public function getProduct() {
        return $this->product;
    }
    
}

class ProductSmall extends CreateItem {
    function __construct() {
        parent::__construct();
        parent::setId(uniqid());
    }
}

class ProductBig extends CreateItem {
    function __construct() {
        parent::__construct();
        parent::setId(uniqid());
    }    
    
    public function setWidth($width){
        $this->product['width'] = $width;
    }
    
    public function setHeight($height){
        $this->product['height'] = $height;
    }
    
}

class Attribute {
    public static function getWidth(){
        return 100;
    }
    
    public static function getHeight(){
        return 50;
    }
}

class Builder {
    
    public function buildSmallProduct(){
        $product = new ProductSmall;
        $product->setName("Small");
        $product->setPrice(1000);
        return $product->getProduct();
    }
    
    public function buildBigProduct(){
        $product = new ProductBig;
        $product->setName("Big");
        $product->setPrice(5000);
        $product->setWidth(Attribute::getWidth());
        $product->setHeight(Attribute::getHeight());
        return $product;
    }
    
}