<?php

namespace patterns\templatemethod;

abstract class AbstractTemplateMethod {
    
    abstract public function firstMethod();
    abstract public function secondMethod();

    public function shareMethod(){
        return __METHOD__;
    }
    
}


class AbstractItemOne extends AbstractTemplateMethod {
    
    public function firstMethod() {
        return __METHOD__;
    }

    public function secondMethod() {
        return __METHOD__;
    }  
    
}

class AbstractItemTwo extends AbstractTemplateMethod {
    
    public function firstMethod() {
        return __METHOD__;
    }

    public function secondMethod() {
        return __METHOD__;
    }  
    
}

class TemplateMethod {
    
    private $items = [];
    
    function __construct() {
        $this->items[] = new AbstractItemOne;
        $this->items[] = new AbstractItemTwo;
        return $this->getItems();
    }
    
    private function getItems(){
        $out = "";
        
        foreach ($this->items as $item){
            $out.= $item->firstMethod();
            $out.= $item->shareMethod();
            $out.= $item->secondMethod();
        }
        
        return $out;
    }
    
}