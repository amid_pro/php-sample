<?php

namespace patterns\adapter;

class ClassWithLongNames {
    
    public function veryLongMethodInsteadGetMethod(){
        return __METHOD__;
    }
    
    public function veryLongMethodInsteadgetClass(){
        return __CLASS__;
    }
    
}

class Adapter {
    
    private $class_example;
            
    function __construct() {
        $this->class_example = new ClassWithLongNames;
    }
    
    function getMethod(){
        return $this->class_example->veryLongMethodInsteadGetMethod();
    }
    
    function getClass(){
        return $this->class_example->veryLongMethodInsteadgetClass();
    }
    
}

